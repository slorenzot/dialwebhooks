<?php
require_once('../../../config.php');

class DAO {

    public static function connect() {
        global $config;
        $database =  $config["dialapplet"]["database"];

        $conn = pg_pconnect(
            sprintf("host=%s port=%s dbname=%s user=%s password=%s", 
                        $database["host"], 
                        $database["port"], 
                        $database["database"], 
                        $database["username"], 
                        $database["password"]));

        if (!$conn) {
            die("Can't connecto to database server, please check config.php file.\n");
        }

        return $conn;
    }

}

class User {

    /**
     * Devuelve el usuario que coincide con el nombre del usuario
     */
    public static function findByUsername($username) {
        $result = pg_query(DAO::connect(), sprintf("SELECT * FROM dialapplet_users WHERE username LIKE '%s' LIMIT 1", $username));

        if (pg_num_rows($result) == 0) return null;
        
        return pg_fetch_array($result);
    }

}

?>