<?php
require_once('../../../models/user.php');
require_once('../../../lib/utils.php');
require_once('../../../lib/Logger.php');
require_once('../../../config.php'); // $config values
require_once('lib/sugar_session.php');
require_once('config.php');  // $hook config values

$testRecord = "38aeac43-75be-22e9-e9d0-5d20061c1511";
$testAgent = "5bb8f0a4-bdb8-bd0a-3a2c-59946b3d79f1";

class Service {

    public static function assign($recordId, $agentId) {
        global $hook;
        global $testRecord;
        global $testAgent;
        $sugar_action = "set_entry";
        $sugar_url = $hook["sugar"]["api"];
        $sugar_status = "Assigned";
        $sugar_module = "Leads";
        $session = SugarSession::start();
        $logger = new Logger("./log");

        if ($session == null) {
            notFound();
            redirect('static/notfound.php?msg=Usuario no encontrado');
            die('User not found!');
        }

        $session_id = $session->id;

        $action_result = call($sugar_action, array(
            "session" => $session_id,
            "module_name" => $sugar_module,
            "name_value_list" => array(
                    array(
                        "name" => "id", 
                        "value" => $recordId
                    ),
                    array(
                        "name" => "status", 
                        "value" => $sugar_status
                    ),
                    array(
                        "name" => "assigned_user_id", 
                        "value" => $agentId
                    ),
                ),
            ), $sugar_url);
        
        $logger->debug("Call action ${sugar_action} => ${sugar_url} with payload => ".print_r($set_entry_parameters, true));
        $logger->debug("Response from action => ".print_r($action_result, true));
        return $action_result;
    }

    public static function open($params) {
        global $config; // global config
        global $hook; // hook config
        $requestId = uniqid(); // Request UID
        $clientIP = getUserIpAddr();

        try {
            $recordId = setOrVal($params['rcid'], "");
            $dialappletUser = setOrVal($params['dusr'], "");
            // $destinationUrl = setOrVal($params['dst'], "");
            $test = isset($params['test']);
            // $https = isset($params['https']);
        } catch(Exception $e) {
            notFound();
            redirect('static/notfound.php?msg=Faltan parametros');
            die('Some parameters not found!');
        }
    
        if (!in_array($clientIP, $config["dialapplet"]["allowfrom"], false)) {
            rejected();
            die(sprintf("IP %s source not allowed in config", $clientIP));
        }
    
        $user = User::findByUsername($dialappletUser);
    
        if ($user == null) {
            notFound();
            redirect('static/notfound.php?msg=Usuario no encontrado');
            die('User not found!');
        }

        $agentId = $user['externname'];
        Service::assign($recordId, $agentId);

        $baseUrl = $hook["sugar"]["actions"]["open_record"];
        $serviceUrl = sprintf($baseUrl, "38aeac43-75be-22e9-e9d0-5d20061c1511");
    
        $session = SugarSession::start();
        if ($test) die(sprintf("Testing redirection to %s", $serviceUrl));

        redirect($serviceUrl);
    }

    public static function processRequest($params) {
        Service::open($params);
    }
}

Service::processRequest($_GET);

?>