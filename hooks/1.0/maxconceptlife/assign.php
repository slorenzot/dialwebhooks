<?php
require_once('../../models/user.php');
require_once('../../lib/utils.php');
require_once('../../../config.php');
/* $estado, Valor texto que identifica el estado del Prospecto los valores que debe enviar el sistema telefonico son
Assigned = Cuando el Prospecto es asignado a un Agente
marcado = Cuando el prospecto es marcado pero no tubo interaccion con un Agente
*/
$estado = "Assigned";

/* $idAgente, Valor Texto que contiene el ID del Agente en el CRM */
$idAgente = "5bb8f0a4-bdb8-bd0a-3a2c-59946b3d79f1";

/* $idrecord, Valor Texto que contiene el ID del Prospecto en el CRM */
$idrecord = "38aeac43-75be-22e9-e9d0-5d20061c1511";

$modulo = "Leads";
$url = "http://50.112.8.136/zakara/service/v4_1/rest.php";
// $username = "dialapplet";
// $password = "ba357b246eaff61c0aa40721e8c2e6f7";

$clientId = setOrVal($params['clid'], "");
$dialappletUser = setOrVal($params['dusr'], "");
$destinationUrl = setOrVal($params['dst'], "");

class Service {

    public static function processRequest($method, $parameters, $url) {
        $session = Service::remoteLogin();

        if ($session != null) {
            notFound();
            redirect('static/notfound.php?msg=Usuario no encontrado');
            die('User not found!');
        }

        $sessid = $session->id;

        $name_value_list = array(
            array("name" => "id", "value" => $idrecord),
            array("name" => "status", "value" => $estado),
            array("name" => "assigned_user_id", "value" => $idAgente),
        );
        $set_entry_parameters = array(
                "session" => $session_id,
                "module_name" => $modulo,
                "name_value_list" => $name_value_list,
        );
        $set_entry_result = call("set_entry", $set_entry_parameters, $url);
    }

}

?>
