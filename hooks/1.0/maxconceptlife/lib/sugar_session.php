<?php
require_once('config.php');
require_once('../../../lib/Logger.php');

class SugarSession {

    public static function start() {
        global $hook;
        $sugar_url = $hook["sugar"]["api"];
        $sugar_action = "login";
        $sugar_username =  $hook["sugar"]["username"];
        $sugar_password = $hook["sugar"]["password"];
        $sugar_payload = array(
            "user_auth" => array(
                "user_name" => $sugar_username,
                "password" => $sugar_password,
                "version" => "1"
            ),
            "application_name" => "RestTest",
            "name_value_list" => array(),
        );

        $logger = new Logger("./log");

        $action_result = call($sugar_action, $sugar_payload, $sugar_url);

        $logger->debug("Call action ${sugar_action} => ${sugar_url} with payload => ".print_r($sugar_payload, true));
        $logger->debug("Response from action => ".print_r($action_result, true));
        return $action_result;
    }

}


?>