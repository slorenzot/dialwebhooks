<?php
    $message = isset($_GET['msg']) ? $_GET['msg'] : "Error lanzando Webhook URL";
?>
<html>
    <head>
        <title>Redirección fallida...</title>
    <link rel="stylesheet" href="styles.css" />
    </head>
    <div id="main">
            <div class="fof">
                    <h1>
<?php
    echo $message;
?>
                    </h1>
                    <p>Si este mensaje persiste, por favor póngase en contacto con el equipo técnico</p>
            </div>
    </div>
</html>