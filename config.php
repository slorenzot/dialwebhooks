<?php
/**
 * Configuraciones para la conexion con la base de datos
 */
$config = array(
    "dialapplet" => array(
        "allowfrom" => array(
            "127.0.0.1",
            "::1"
        ),
        /**
         * Credenciales para la conexion con la base de datos de dialapplet
         */
        "database" => array(
            "host" => "localhost",
            "port" => "5432",
            "database" => "dialapplet",
            "username" => "pgadmin",
            "password" => "pgadmin")
    )
);

?>
