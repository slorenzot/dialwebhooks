<?php

function setOrVal($value, $other) {
    try {
        return isset($value) ? $value : $other;
    } catch(Exception $e) {}
    return $other;
}

function getUserIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

/**
 * Envia las cabeceres de error 401
 */
function rejected() {
    header("HTTP/1.1 401 Unauthorized", false, 401);
}

/**
 * Envia las cabeceras de error 404
 */
function notFound() {
    header("HTTP/1.0 404 Not Found", false, 404);
}

/**
 * Forza la redirección a la URL indicada
 */
function redirect($url) {
    Header('Status: 301 Moved Permanently', false, 301);
    Header(sprintf("Location: %s", $url));
}


function call($method, $parameters, $url) {
    ob_start();
    $curl_request = curl_init();
    curl_setopt($curl_request, CURLOPT_URL, $url);
    curl_setopt($curl_request, CURLOPT_POST, 1);
    curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($curl_request, CURLOPT_HEADER, 1);
    curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
    $jsonEncodedData = json_encode($parameters);
    $post = array(
            "method" => $method,
            "input_type" => "JSON",
            "response_type" => "JSON",
            "rest_data" => $jsonEncodedData
    );
    curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec($curl_request);
    curl_close($curl_request);
    $result = explode("\r\n\r\n", $result, 2);
    $response = json_decode($result[1]);
    ob_end_flush();
    return $response;
}

?>