<?php

/**
 * Class Log
 * A really simple logging class that writes flat data to a file.
 * @author Dennis Thompson
 * @license MIT
 * @version 1.0
 * @copyright AtomicPages LLC 2014
 */
class Logger {

	private $handle, $dateFormat;

	public function __construct($file, $mode = "a") {
		try {
			$this->handle = fopen($file, $mode);
			$this->dateFormat = "d/M/Y H:i:s";
		} catch(Exception $e) {}
	}

	public function dateFormat($format) {
		$this->dateFormat = $format;
	}

	public function getDateFormat() {
		return $this->dateFormat;
	}

	public function debug($entry) {
		$this->log("DEBUG", $entry);
	}
	
	public function warning($entry) {
		$this->log("WARNING", $entry);
	}

	public function error($entry) {
		$this->log("ERROR", $entry);
	}

	/**
	 * Writes info to the log
	 * @param mixed, string or an array to write to log
	 * @access public
	 */
	public function log($type, $entries) {
		try {
			if(is_string($entries)) {
				fwrite($this->handle, $type.": [" . date($this->dateFormat) . "] " . $entries . "\n");
			} else {
				if (is_array($entries)) foreach($entries as $value) {
					fwrite($this->handle, $type.": [" . date($this->dateFormat) . "] " . $value . "\n");
				}
			}
		} catch(Exception $e) {}
	}

}